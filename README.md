# lanca-valor

Sistema para realizar lançamentos de entrada e saídas (Débito e Crédito) e gera um relatório consolidado.

Para rodar o sistema é necessário ter o java 11 instalado e o maven.

Realizar o clone do projeto e rodar o comando:

    mvn clean install

Após a execução para rodar o aplicação usar o comando:

    mvn spring-boot:run

Para incluir lançamentos no sistema basta realizar uma requisição post para o sistemas, exemplo:

    `curl --location 'http://localhost:9001/service/lancavalores/diario' \
--header 'Content-Type: application/json' \
--data '{
    "descricao" : "VENDA",
    "valor": 10500,
    "tipoLancamento": "DEBITO",
    "usuario": "Marcos"
}'
`

Para gerar as informações do consolidado usar o request get abaixo:

`curl --location 'http://localhost:9001/service/lancavalores/diario'`


Foi configurado o docker para a criação da imagem do sistema no dockerfile e precisa criar o arquivo de configuração do kubernates utilizando as informações para ter a possibilidade de escalar o sistema.

Ainda pode ser melhorado o sistema colocando o swagger para facilitar as chamadas.


Obs. Desenho técnico incluso no git na pasta documento
