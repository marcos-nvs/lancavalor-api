# Usa a imagem base do Java 11
FROM adoptopenjdk:11-jdk-hotspot

# Define o diretório de trabalho no contêiner
WORKDIR /app

# Copia o arquivo .jar compilado para o diretório de trabalho
COPY target/valores-0.0.1.jar /app/app.jar

# Define o comando padrão para executar o aplicativo Spring Boot
CMD ["java", "-jar", "app.jar"]