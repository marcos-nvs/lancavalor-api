package br.com.lanca.valores.service.impl;

import br.com.lanca.valores.domain.ConsolidadoDTO;
import br.com.lanca.valores.domain.DiarioDTO;
import br.com.lanca.valores.entity.Diario;
import br.com.lanca.valores.repository.DiarioRepository;
import br.com.lanca.valores.service.DiarioService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DiarioServiceImpl implements DiarioService {

    @Autowired
    private DiarioRepository repository;

    @Override
    public DiarioDTO insereValor(DiarioDTO diarioDTO) {

        Diario diario = new Diario();

        BeanUtils.copyProperties(diarioDTO, diario);
        diario.setDataLancamento(LocalDateTime.now());
        diario = repository.save(diario);
        BeanUtils.copyProperties(diario, diarioDTO);
        return diarioDTO;
    }

    @Override
    public ConsolidadoDTO getRelatorioConsolidado() {

        ConsolidadoDTO consolidado = new ConsolidadoDTO();
        consolidado.setEntradas(new BigDecimal(0));
        consolidado.setSaidas(new BigDecimal(0));
        consolidado.setSaldo(new BigDecimal(0));

        List<Diario> listDiario = repository.findAll();
        listDiario.stream().forEach(diario -> {

            switch (diario.getTipoLancamento()) {
                case DEBITO:
                    consolidado.setEntradas(consolidado.getEntradas().add(diario.getValor()));
                    break;
                case CREDITO:
                    consolidado.setSaidas(consolidado.getSaidas().add(diario.getValor()));
                    break;
            }

        });

        consolidado.setSaldo(consolidado.getEntradas().subtract(consolidado.getSaidas()));

        return consolidado;
    }


}
