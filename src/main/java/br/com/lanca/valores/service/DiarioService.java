package br.com.lanca.valores.service;

import br.com.lanca.valores.domain.ConsolidadoDTO;
import br.com.lanca.valores.domain.DiarioDTO;

import java.util.List;

public interface DiarioService {
    DiarioDTO insereValor(DiarioDTO diarioDTO);

    ConsolidadoDTO getRelatorioConsolidado();
}
