package br.com.lanca.valores.enuns;

public enum TipoLancamento {
    DEBITO, CREDITO;
}
