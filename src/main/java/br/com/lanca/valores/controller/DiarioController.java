package br.com.lanca.valores.controller;

import br.com.lanca.valores.domain.ConsolidadoDTO;
import br.com.lanca.valores.domain.DiarioDTO;
import br.com.lanca.valores.service.DiarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/diario")
public class DiarioController {

    @Autowired
    private DiarioService service;

    @PostMapping
    public ResponseEntity<DiarioDTO> insereValor(@RequestBody @Valid DiarioDTO diarioDTO){
        return ResponseEntity.ok().body(service.insereValor(diarioDTO));
    }

    @GetMapping
    public ResponseEntity<ConsolidadoDTO> getRelatorioConsolidado(){
        return ResponseEntity.ok().body(service.getRelatorioConsolidado());
    }

}
