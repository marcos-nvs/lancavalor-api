package br.com.lanca.valores.repository;

import br.com.lanca.valores.entity.Diario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DiarioRepository extends JpaRepository<Diario, UUID> {

}
