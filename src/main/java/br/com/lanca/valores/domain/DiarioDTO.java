package br.com.lanca.valores.domain;

import br.com.lanca.valores.enuns.TipoLancamento;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DiarioDTO {

    private UUID id;

    @NotBlank(message = "descrição é obrigatório")
    private String descricao;
    @NotNull(message = "tipoLancamento é obrigatório (DEBITO/CREDITO)")
    private TipoLancamento tipoLancamento;
    @NotNull(message = "valor é obrigatório")
    private BigDecimal valor;
    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss")
    private LocalDateTime dataLancamento;
    @NotBlank(message = "usuario é obrigatório")
    private String usuario;
}
