package br.com.lanca.valores.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConsolidadoDTO {

    private BigDecimal entradas;
    private BigDecimal saidas;
    private BigDecimal saldo;


}
