package br.com.lanca.valores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValoresApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValoresApiApplication.class, args);
	}

}
