package br.com.lanca.valores.entity;

import br.com.lanca.valores.enuns.TipoLancamento;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "diario")
@Data
public class Diario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Column(name = "tipo", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoLancamento tipoLancamento;

    @Column(name = "valor", nullable = false)
    private BigDecimal valor;

    @Column(name = "dataLancamento", nullable = false)
    private LocalDateTime dataLancamento;

    @Column(name = "usuario", nullable = false)
    private String usuario;
}
